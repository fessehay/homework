import { Component } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent  {

  firstInput:number;
  secondInput:number;

  get addition(){
    if( this.firstInput !== null && this.secondInput !== null){
      return this.firstInput + this.secondInput
    }
    else {
      return 0
    }
  }
  get subtraction(){
    if( this.firstInput !== null && this.secondInput !== null){
      console.log(this.firstInput,this.secondInput,this.firstInput - this.secondInput)
      return this.firstInput - this.secondInput
    }
    else {
      return 0
    }
  }

  get multiplication (){
    if( this.firstInput !== null && this.secondInput !== null){
      return this.firstInput * this.secondInput
    }
    else {
      return 0
    }
  }
}
