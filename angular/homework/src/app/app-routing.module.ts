import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
 
import { CalculatorComponent } from './calculator/calculator.component';
import { PalindromeComponent } from './palindrome/palindrome.component';
 
const routes: Routes = [
  { path: '', redirectTo: '/palindrome', pathMatch: 'full' },
  { path: 'calculator', component: CalculatorComponent },
  { path: 'palindrome', component: PalindromeComponent }
];
 
@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}