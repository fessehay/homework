import { Component } from '@angular/core';

@Component({
  selector: 'app-palindrome',
  templateUrl: './palindrome.component.html',
  styleUrls: ['./palindrome.component.css']
})

export class PalindromeComponent {

  palindromeText:string = "";

  get palindromeSolution(){
    let palText = this.palindromeText.toLowerCase().replace(/[\W_]/g, '');
    return (
      palText ===
      palText
        .split('')
        .reverse()
        .join('') ? "Palindrome" : "Not a Palindrome"
    );
  } 

}
