# Back end challenges

The challenges here are probably harder than in the front end.
Basic express and graphql functionality is already here.

# CHOOSE ONE - from least difficult to most difficult

  - Create a graphql query to make a user. - *least difficult*
  - Create a moongoose model hook so passwords are hashed.
  - Create a login mutation in graphql. - *most difficult*
  - Any other functionality that might be useful.

# Setup

Either setup with Docker or MongoDB

## Docker

To run this you just need to serveral technolgies

[For ubuntu](https://docs.docker.com/engine/installation/linux/ubuntu/)

- docker
- docker compose

After installing this you will need to run

```
docker-compose up --build
```

or

```
docker-compose build
docker-compose up
```

## With mongoDb

Install mongoDB individually on your computer and get the database running.

All you would need to do in this case is to run
`npm install`
`npm run dev`

## To use the graphiql ide:

Go to [http://localhost:4000/graphiql](http://localhost:4000/graphiql)

# Reference Libraries

- [https://github.com/nodkz/graphql-compose-mongoose](https://github.com/nodkz/graphql-compose-mongoose)

- [https://github.com/nodkz/graphql-compose](https://github.com/nodkz/graphql-compose)

- [https://github.com/apollographql/graphql-server](https://github.com/apollographql/graphql-server)

